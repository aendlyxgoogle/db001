print(f'#$!> manjaro_i3_k4z4r_bootstrap')
print(f'starting_yaourt_part_#1')

import subprocess
import sys

packages = [
    'terminator',
    'emacs',
    'code',
    'docker',
    'firefox',
    'chromium',
    'google-chrome',
    'glances',
    'openvpn',
]
def add_packages(packages):
    for arg in sys.argv[1:]:
        packages.append(arg)
    return packages
packages = add_packages(packages)

commands = [ ['yaourt', '-Syu'] ]
def add_commands(commands, packages):
    for name in packages:
        commands.append(['yaourt', '-S', name])
    return commands
commands = add_commands(commands, packages)
def call_commands(commands):
    for cmd in commands:
        print(f'#$!> calling : \t {cmd}')
        subprocess.call(cmd)
call_commands(commands)



print(f'starting_curl_part_#2')
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

print(f'starting_custom_part_#3')
# downloads and setup some gitlab repository or working env